package stepDef.APIStepDef;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import users.CreateUserWithLombok;
import users.UpdateUserWithLombok;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.ApiUtils.serializePOJO;
import static utils.ConfigReader.getProperty;

public class ReqResStepDef {

    Response response;
    int actualUserId;

    @Given("Create user with {string} and {string}")
    public void createUserWithAnd(String name, String job) {
        CreateUserWithLombok createUserWithLombok = CreateUserWithLombok
                .builder().name(name).job(job).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(createUserWithLombok))
                .post(getProperty("ReqResURL") + "/api/users/")
                .then().log().all().extract().response();

        actualUserId = Integer.parseInt(JsonPath.read(response.asString(), "id"));

    }

    @And("Validate that status code is {int}")
    public void validateThatStatusCodeIs(int expectedStatusCode) {

        int actualStatusCode = response.getStatusCode();

        assertThat(
                "I am expecting status code: " + expectedStatusCode,
                actualStatusCode,
                is(expectedStatusCode)
        );
    }

    @And("Make GET call to get user with {string}")
    public void makeGETCallToGetUserWith(String url) {

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .get(url + actualUserId)
                .then().log().all().extract().response();

    }

    @And("Updating the user with the following data")
    public void updatingTheUserWithTheFollowingData(Map<String, String> data) {

        UpdateUserWithLombok updateUserWithLombok = UpdateUserWithLombok
                .builder().name(data.get("name"))
                .job(data.get("job")).build();


        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(updateUserWithLombok))
                .put(getProperty("ReqResURL") + "/api/users/" + actualUserId)
                .then().log().all().extract().response();

    }

    @When("I delete user")
    public void iDeleteUser() {

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .delete(getProperty("ReqResURL") + "/api/users/" + actualUserId)
                .then().log().all().assertThat().statusCode(204).extract().response();
    }


}