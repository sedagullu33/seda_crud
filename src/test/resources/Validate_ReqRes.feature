

Feature: As a QE, I validate the Reqres API CRUD operation

  Scenario Outline: Validating the ReqRes API CRUD operation
    Given Create user with "<name>" and "<job>"
    And Validate that status code is 201
    And Make GET call to get user with "<URL>"
    And Validate that status code is 200
    And Updating the user with the following data
      | name | seda |
      | job  | sdet |
    And Validate that status code is 200
    When I delete user
    Then Validate that status code is 204

    Examples: GoRest data
      | name | job  | URL                          |
      | seda | sdet | https://reqres.in/api/users/ |